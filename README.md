# sault

**S**tatic **a**nalysis of **u**nits by **l**everaging **t**emplates

Sault is a C++ library that employs template meta-programs to statically
analyze the use of units in calculations.  Incorrect or mismatched units often
lead to significant bugs, sometimes with catastrophic results.  Sault utilizes
the C++ type system to ensure units are correct without impacting runtime
performance.

## License

Sault is licensed under the terms of the GNU Lesser General Public
License (LGPL) version 3 or later.
